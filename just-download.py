import pycurl
import time
import sys

def download_url (url):
    with open ("/dev/null",'wb') as f:
        c=pycurl.Curl ()
        c.setopt (pycurl.URL,url)
        c.setopt (pycurl.WRITEDATA,f)
        st=time.time ()
        c.perform ()
        et=time.time ()
        print (c.getinfo (pycurl.NAMELOOKUP_TIME))
        print (c.getinfo (pycurl.CONNECT_TIME))
        print (c.getinfo (pycurl.STARTTRANSFER_TIME))
        print (c.getinfo (pycurl.TOTAL_TIME))
        print (c.getinfo (pycurl.CONTENT_LENGTH_DOWNLOAD))
        c.close ()

args=sys.argv

while True:
    download_url (args [1])